'use strict';

var Alexa = require('alexa-sdk');
var http = require('http');

var alexa;

//
// CONSTANTS
//
const SERVICE_HOST = 'home.tylxr.com',
      SERVICE_PORT = 60802;

var downloadHandlers = {
  'downloadIntent': function () {
    var mediaQuery = this.event.request.intent.slots.MediaQuery.value;
    var service = this.event.request.intent.slots.Service.value.toLowerCase();

    var webServicePath = '/download/';

    switch(service) {
      case 'movies':
        webServicePath += 'tpb/movies/'
        break;
      case 'tv shows':
        webServicePath += 'tpb/tvshows/';
        break;
    }

    webServicePath += mediaQuery;
    httpGet(encodeURI(webServicePath), function(response) {
      var voiceOutput = '';
      var responseData = JSON.parse(response);
      if (responseData == null) {
        voiceOutput = 'Siri is currently not speaking to me';
      }
      else {
        if(responseData.success) {
          voiceOutput = 'Downloading ' + mediaQuery + ' from ' + service;
        } else {
          voiceOutput = responseData.message; //'Siri was unable to respond to your request';
        }
      }

      alexa.emit(':tell', voiceOutput);
    });

    //alexa.emit(':tell', 'test');
    
  }
}

exports.handler = function (event, context, callback) {
    alexa = Alexa.handler(event, context);
    alexa.registerHandlers(downloadHandlers);
    alexa.execute();
};

// Create a web request and handle the response.
function httpGet(query, callback) {
  console.log("/n QUERY: "+query);

    var options = {
        host: SERVICE_HOST,
        port: SERVICE_PORT,
        path: query,
        method: 'GET'
    };

    var req = http.request(options, (res) => {

        var body = '';

        res.on('data', (d) => {
            body += d;
        });

        res.on('end', function () {
            callback(body);
        });

    });
    req.end();

    req.on('error', (e) => {
        console.error(e);
    });
}