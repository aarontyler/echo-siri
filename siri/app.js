'use strict';

//
// IMPORTS
//

// Include shelljs commands so we can execute bash commands
require('shelljs/global');

require('xmlhttprequest');

// Include applescript so we can run scripts
var applescript = require('applescript');

// Include fs so we can access the file system
var fs = require('fs');

// Not sure what util is for??
var util = require('util');

// Express web server
var express = require('express');

// The Pirate Bay
var thePirateBay = require('thepiratebay');

// uTorrent
var uTorrent = require('library-utorrent');

// Include mdns so we can advertise the service - but for who?
var mdns = require('mdns');

// Xbox control
var Xbox = require('xbox-on');

// Express instance
var app = express();

var Remote = require('./unit508-tv-remote');
var remote = Remote();

//
// CONSTANTS
//

const SERVICE_PORT					= 60802,

// uTorrent settings
      UTORRENT_HOST 				= 'imac.home.tylxr.com',
			UTORRENT_PORT 				= 43611,
			UTORRENT_USER 				= 'tyler',
			UTORRENT_PASS 				= '24me04u',
			UTORRENT_PATH_MOVIES 	= 'Movies/',
			UTORRENT_PATH_TVSHOWS = 'TV Shows/',

// The Pirate Bay categories
		  TPB_CATEGORY_MOVIES 	= 207,
			TPB_CATEGORY_TVSHOWS 	= 208,

// Xbox settings
			XBOX_HOST 						= 'xboxone.home.tylxr.com',
			XBOX_LIVE_ID 					= '';

//
// OBJECTS
//
// remote.wake(function(done) {
// 	console.log ('wake result: ' + done);
// });
// remote.api_active(function(isActive) {
// 	console.log('API Active: ' + isActive);
// });
// remote.sendKey('KEY_VOLDOWN', function(err, res) {
// 	console.log(err);
// 	console.log(res);
// 	// if (err) {
// 	// 	console.log(err);
// 	// } else {
// 	// 	console.log(res);
// 	// }
// });

// uTorrent Downloader
var downloader = {};
downloader.startTorrent = function(urlOrMagnet, path, res) {
	var success = false;
	var message = '';
	uTorrent.addTorrentUrl({
		host: UTORRENT_HOST,
		port: UTORRENT_PORT,
		username: UTORRENT_USER,
		password: UTORRENT_PASS,
		torrentUrl: urlOrMagnet,
		downloadDir: 0,
		path: path,
	}).exec({
		// An unexpected error occurred. 
		error: function (err){
			console.log(err);
			//return {'success':'false'};
			//res.json(err);
			res.json({ 'success': false, 'message': err });
		},
		// OK. 
		success: function (){
			console.log('success');
			res.json({ 'success': true, 'message': 'Downloading...' });
			//return ({'success':'true'});
		}
	});
	return 
}

//var bodyParser = require('body-parser');

//
// API ROUTES
//

// Create File request
// app.get('/createfile', function(req, res) {
// 	console.log('createfile called...');
// 	fs.writeFile('/testfile.txt', 'some file contents');
// 	res.json({'success':'success'});
// });

// Launch netflix request
app.get('/netflix', function(req, res) {
	var tvApp;
	console.log('netflix called...');
	try {
		remote.launchApp('org.tizen.netflix-app', function() {
			res.json({'success':true});
		}, 
		function(error) {
			res.json({'success':false,
								'message': error });
		});
		
	} catch (error) {
		console.log('EXCEPTION:');
		console.log(error);
		res.json({'success':'no'});
	}
});
// Control TV Power
app.get('/tv/power/:state', function (req, res){
	try {
		if(req.params.state === 'on') {
			exec('echo on 0 | cec-client -s -d 1 | ssh -i /Users/aarontyler/.ssh/rpi_id_rsa pi@rpi');
			res.json({ 'success': true });
		} else {
			exec('echo on 0 | cec-client -s -d 1 | ssh -i /Users/aarontyler/.ssh/rpi_id_rsa pi@rpi');
		}
	}
	catch (err) {
		res.json({ 'success': false, 
								'message': err.message	
		});
	}
}),
// Launch TV app
app.get('/tv/launchapp/:appname', function(req, res) {
	console.log('Received request to launch ' + req.params.appname + ' on the TV...');
	try {
		remote.launchApp(req.params.appname, function() {
			res.json({'success':true});
		}, 
		function(error) {
			res.json({'success':false,
								'message': error });
		});
		
	} catch (error) {
		console.log('EXCEPTION:');
		console.log(error);
		res.json({'success':'no'});
	}
});

app.get('/xbox/on', function(req, res) {
	try {
		var xbox  = new Xbox(XBOX_HOST, XBOX_LIVE_ID);
		xbox.powerOn();
		res.json({'success':true});
	} catch (error) {
		console.log('EXCEPTION:');
		console.log(error);
		res.json({'success':false});
	}
});

app.get('/download/tpb/:category/:query', function(req, res) {
	var searchResults;
	console.log('Searching The Pirate Bay ' + req.params.category + ' for ' + req.params.query + '...');

	var query = req.params.query;
	var dlPath = '';

	if(req.params.category == 'movies') {
		query += ' 1080p';
		dlPath = UTORRENT_PATH_MOVIES;
	} else {
		dlPath = UTORRENT_PATH_TVSHOWS;
	}

	thePirateBay.search(query, {
  	category: req.params.category
	})
	.then(function(results) {
		var dlResponse = downloader.startTorrent(results[0].magnetLink, dlPath, res);
		//res.json(dlResponse);
	})
	.catch(err => res.json(err));
});

// app.get('/download/tpb/tvshows/:query', function(req, res) {
// 	var searchResults;
// 	console.log('Searching The Pirate Bay for ' + req.params.query + '...');
// 	thePirateBay.search(req.params.query, {
//   	category: 207
// 	})
// 	.then(function(results) {
// 		var dlResponse = downloader.startTorrent(results[0].magnetLink, UTORRENT_PATH_TVSHOWS, res);
// 		res.json(dlResponse);
// 	})
// 	.catch(err => res.json(err))
// });

 console.log('Starting echo-siri web service on port ' + SERVICE_PORT + '...');
 app.listen(process.env.PORT || SERVICE_PORT);

 var ad = mdns.createAdvertisement(mdns.tcp('echo-siri'), process.env.PORT || 8080);
 ad.start();

 console.log('echo-siri is now running on port ' + SERVICE_PORT + '.');

// Get all TPB categories:
// thePirateBay.getCategories().then(function (results) {
// 	console.log(Object.keys(results).length);
// 	for(var i in results) {
// 		console.log (results[i]);//results[i].id + ': ' +results[i].name);
// 	}
// });
