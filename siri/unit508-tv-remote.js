const WebSocket = require('ws');
const request = require('request');
var wol = require('wake_on_lan');
var msf = require('./node-msf-2.3.3.js');

module.exports = function () {
  const APP_ID = 'YTaAZgIoK9.Unit508',
    CHANNEL_ID = 'com.samsung.msf.Unit508',
    USERNAME = 'Client',
    TV_IP = '192.168.1.207',
    TV_MAC = '5C:49:7D:64:08:52',
    API_TIMEOUT = 2000,
    TV_PORT = 8001;

  return {
    launchApp: function (targetAppName, onSuccess, onError) {
      function launch(targetAppName, onSuccess, onError) {
        var tvApp;
        msf.remote('http://' + TV_IP + ':' + TV_PORT + '/api/v2/', function (error, service) {
        if (error) {
          console.log("Error occurred while connecting: " + error.message);
          onError(error.message);
        } else {
          console.log('Attempting to connect to TV app...');
          tvApp = service.application(APP_ID, CHANNEL_ID);
          tvApp.connect({ name: USERNAME }, function (err, client) {
            if (err && err.code == "404") {
              console.log("Error connecting to application.");
              // if (confirm("Would you like to install the application in your TV?")) {
              //     app.install(function(err){
              //         if (err) {
              //             Log("Error installing application: " + err.message);
              //         } else {
              //             Log("Please follow installation steps on your TV.");
              //         }
              //     });
              // }
              onError('App is not installed on the TV');
            } else if (err) {
              console.log("An error occurred: " + err.message);
              // if (confirm('Would you like to try to connect to channel ' + chanName + '?\nPlease remember, you need to launch the application manually.')) {
              //     initChannel(device, function(chan){
              //         app = chan;
              //         if (!connected) {
              //             channelOnConnect();
              //         }
              //     });
              // }
              onError(err.message);
            } else {
              console.log("Client connected.");
              setTimeout(function() { tvApp.publish("launchApp", targetAppName ) }, 1000);
              onSuccess();
            }
          });
        }
      });
      }
      
      
      this.isApiActive(function() {
        launch(targetAppName, onSuccess, onError);
      }, function() {
        var that = this;
        wol.wake(TV_MAC, function(error) {
          if (error) {
            onError(error);
          } else {
            setTimeout(function() { launch(targetAppName, onSuccess, onError) }, 3500);
          }
        });
      });
    },
    wake: function (done) {
      wol.wake(mac_address, function (error) {
        if (error) { done(1); }
        else { done(0); }
      });
    },

    isApiActive: function (onSuccess, onError) {
      request.get({ url: 'http://' + TV_IP + ':' + TV_PORT + '/api/v2/', timeout: API_TIMEOUT }, function (err, res) {
        if (!err && res.statusCode === 200) {
          console.log('TV is on');
          onSuccess();
        } else {
          console.log('No response from TV');
          onError();
        }
      });
    }
  }
};